<?php session_start(); ?>
<div class="container">
	<div class="row">
		<div class="card w-100 mb-2">
			<div class="row">
					<p class="card-header">Личные данные</p>
			</div>
				<p class="card-text">
			 		<div class="row">
			 			<label for="" class="col-4 col-form-label">Почта</label>
			 			<div class="col-6">
			 				<input readonly type="text" class="form-control-plaintext" value="example@mail.ru">
			 			</div>
			 		</div>
			 		<div class="row">
			 			<label for="" class="col-4 col-form-label">Телефон</label>
			 			<div class="col-6">
			 				<input readonly type="text" class="form-control-plaintext" value="3752929262">
			 			</div>
			 		</div>
			 		<div class="row">
			 			<label class="col-4 col-form-label">Адрес</label>
			 			<div class="col-6">
			 				<input readonly type="text" class="form-control-plaintext" value="ПРоспект фрунзе 28 ыфовлдфыовлдоыдфвоыд">
			 			</div>
			 		</div>
				</p>
				<div class="card-footer row">
					<button class="btn btn-warning">Изменить</button>
				</div>
			 	
			 
		</div>
		<div class="card" style="width: 100%;">
			<div class="row">
					<p class="card-header">Последние услуги</p>
			</div>
			<div class="card-body">
			    <p class="card-text">
			    	<div>
						<table class="table dark table-striped">
							<thead>
								<tr>
									<th>Товар</th>
									<th>Цена</th>
								</tr>
							</thead>
							<tbody id="main-cart">
								
							</tbody>
						</table>
					</div>
			    	<a href="history" class="btn btn-info w-100">История покупок</a>
			    </p>
		    </div>
			
		</div>
	</div> <!--row-->
</div>