<?php
	session_start(); 
	require 'vendor/function.php';
	global $link;
	$sql = "SELECT * FROM Category";
	$result = $link -> query($sql);
	//$result2 = pagination();

?>
<!--Для уведомлений-->
<div id="myalert"></div>

<div class="container">
	<div class="row mb-3 bg-warning rounded p-1">
		<div class="col-4">
			<select name="" id="category-select" class="form-select" value="Выбрать раздел">
				<option value="0" selected>Выбрать раздел</option>
				<?php 
					while ($row = $result -> fetch_array()) {
						echo "<option value='$row[0]'>$row[1]</option>";
					}
				?>
			</select>

		</div>
		<div class="col-md-4">
			<select name="" class="form-select" id="sorting-select">
				<option value="None">По умолчанию</option>
				<option value="Name ASC">По алфавиту</option>
				<option value="Cost ASC">По возрастанию цены</option>
			</select>
		</div>
		<div class="col-md-4">
			<input class="form-control" type="search" id="search_box" placeholder="Введите для поиска">
		</div>
	</div>
	<div class="row" id="service-block">

	</div>
	<nav aria-label="Page navigation">
	  <ul class="pagination justify-content-center" id="page-links">
	  </ul>
	</nav>
</div>


<script>

	var cart = {};

	$(document).ready(function(){
		loadPage(1); //initial
		
		$(document).on('click', '#ToCart', sendInvite);

		$(document).on('click', '.page-link', pageRole);
		$(document).on('change','#category-select', filter);
		$(document).on('change','#sorting-select', sorting);
	    
	    loadCart();
	    showMiniCart();

		$('[data-toggle="popover"]').popover();
		$('#search_box').keypress(search);

	});
	
	function getPageData(){
		const filter = $('#category-select').val();
		const query = $('#search_box').val();
		const sort = $('#sorting-select').val();
		
		const obj = {
			filter: +filter ? filter : undefined,
			query,
			sort,
		};
		loadPage(1, obj.query, obj.filter, obj.sort);
		console.log(obj);
	}
	function sorting(){
		getPageData();
	}

	function filter(){

		const filter = $('#category-select').val();
		const query = $('#search_box').val()
		if(filter === '0') {
			loadPage(1, query)
		} else {
			loadPage(1, query, filter)
		}
		
		//Фильтрация
		/* $.get(
			"templates/vendor/core.php",
			{
				"filter" : $('#category-select').val(),
				"action" : "pagination"
			},
			function(data){
				var query = $('#search_box').val();
			
   	  		loadPage(1, query);	
			}
					
	) */}

	function search(e){
		//Поиск
		if (e.which == 13){
			var query = $('#search_box').val();
   	  		loadPage(1, query);				
		}
    };

	function pageRole(){
		//Переключение страниц
		var page = $(this).attr('data-page');
		loadPage(page);
	}

	function saveCart() {
	    //сохраняю корзину в localStorage
	    localStorage.setItem('cart', JSON.stringify(cart)); //корзину в строку 
	}

	function loadPage(page, query = '', categoryId, sortValue){
		console.log(sortValue);
		$.post(
            "templates/vendor/core.php",
            {
                "action" : "pagination",
                "page"   : page,
                "query"  : query,
								"filter" : categoryId,     
								"sort"   : sortValue,
            },
                function(data){

                	var data = JSON.parse(data);
                	viewService(data);
                }
            );

		
	}

	
	function viewService(data)
	{
		//Выгружаем страницу
		var out =``;
		console.log('asdfasdf', data);
		for (var key in data[0])
		out += `<div class="col-auto">
					<div class="card service" style="width: 300px;">
						<img class="card-img-top" src="img/${data[0][key][6]}" alt=""/>
						<ul class="list-group list-group-flush">
							<li class="list-group-item text-wrap"><a title="${data[0][key][1]}" class="text-decoration-none" href="service?id=${data[0][key][0]}">${data[0][key][1]}</a></li>	
							<li class="list-group-item">${data[0][key][2]} ${data[0][key][8]}</li>
							<li class="list-group-item text-muted">${data[0][key][4]}</li>
						</ul>
								
							<div class="card-body p-0">
								<button class="btn bg-dark text-light w-100" id="ToCart" data-id="${data[0][key][0]}">Подать заявку</button>
							</div>
						
					</div>
					
				</div>`;

		$('#service-block').html(out);
		viewPageLink(data);

	}
	
	function viewPageLink(data){
		//выгружаем кнопки страниц
		var out = '';
		for(var i = 1; i <= data[1]; i++)
        	out += `<li class="page-item"><a class="page-link" data-page="${i}">${i}</a></li>`;
		$('#page-links').html(out);
	}


	//добавление в корзину
	function addToCart() {
	    //добавляем товар в корзину
	    var id = $(this).attr('data-id');
	    // console.log(id);
	    $(this).attr('disabled', true);
	    if (cart[id] == undefined) {
	        cart[id] = 1; 
	        //если в корзине нет товара - делаем равным 1        
	    }

	    showAlert("Добавлено в корзину");
	    showMiniCart();
	    saveCart();
	    setCount();

	}

	function sendInvite(){
		var id = $(this).attr('data-id');
		$.post(
			"templates/vendor/core.php",
			{
				action : "sendInvite"
			},
			function(data){
				console.log(data);
				showAlert("Заявка отправлена");
			}
			)
	}

	function showMiniCart() {
	    //показываю мини корзину
	    var out="";
	    let count = 0;
	    for (var key in cart) {
	        out += key +' --- '+ cart[key]+'<br>';
	        count += cart[key];
	    }
	  
	    setCount();
	    saveCartCount(count);  
	}

	function loadCart() {
	    //проверяю есть ли в localStorage запись cart
	    if (localStorage.getItem('cart')) {
	        // если есть - расширфровываю и записываю в переменную cart
	        cart = JSON.parse(localStorage.getItem('cart'));
	        showMiniCart();
	    }
	}
</script>