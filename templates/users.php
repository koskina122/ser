 <?php session_start(); 
require 'vendor/connection.php';
/*
if(isset($_GET['page']))
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = 1;
    }
   
*/
?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>Пользователи</h2>
                </div>
                <div class="col-md-1 text-right">
                    <button type="button" fname="addTovar" data-toggle="modal" data-target="#modalUser" id="addTovar" class="btn btn-success btn"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="main-table"></table>
        </div>       
    </div>
	
    
    
    <div class="modal fade" id="modalUser" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Редактирование</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Имя 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" fname="fnameUser" id="fnameUser" class="form-control" placeholder="Наименование" required >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Фамилия 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" fname="L_fnameUser" id="L_fnameUser" class="form-control"  placeholder="Цена" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Почта 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="email" fname="mailUser" id="mailUser" class="form-control" required placeholder="Почта">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Телефон 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" fname="phoneUser" id="phoneUser" class="form-control" placeholder="Телефон" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Город 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text"  placeholder="Город" fname="cityUser" id="cityUser" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Адрес
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" fname="adressUser" id="adressUser" class="form-control" required placeholder="Адрес">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Статус 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">

                                <select name="statusUser" id="statusUser" class="form-control">
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="hgrp" hidden>
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Пароль 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">

                                <input name="passUser" id="passUser" class="form-control" placeholder="Пароль">
                            
                            </div>
                        </div>
                    </div>  
                </form>  
            </div>
            <div class="modal-footer">
              <a href="" data-dismiss="modal" class="btn">Отмена</a>
              <a class="btn btn-success" id="wrckbtn" data-edit="false" data-dismiss="modal">Ок</a>
            </div>
          </div>
        </div>
    </div>

</div>

<!-- *****************
        
********************** -->
<script>
	$(document).ready(function(){
        init();
        $(document).on('click', '#modaledit', function(){
            var id = $(this).attr('data-id');
            clearInput();
            fillSelect('position',$('#statusUser'));
            loadOne('users', id, setTovar);
            $('#hgrp').attr('hidden', true);
            $('#wrckbtn').attr('data-id', id);
            $('#wrckbtn').attr('data-edit', 'true');
           // $('#modalUser').modal('toggle');
            console.log('tik');
        });

        $(document).on('click', '#addTovar', function(){
            clearInput();
            $('.modal-title').text('Добавить');
            $('#hgrp').removeAttr('hidden');
            fillSelect('position',$('#statusUser'));
            $('#wrckbtn').attr('data-edit', 'false');
         });

        $(document).on('click', '#wrckbtn', sendTovar);
	});

    //Получаем одну запись
    function loadOne(table, id, func){
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOne",
                "table": table,
                "id": id                
            },
                func
            );
    }

    function clearInput(){
        $('input').val("");
    }

    //устанавливаем поля для редактирования товара
    function setTovar(data){
        data = JSON.parse(data);
        $('#fnameUser').val(data[1]);
        $('#L_fnameUser').val(data[2]);
        $('#mailUser').val(data[3]);
        $('#phoneUser').val(data[4]);
        $('#cityUser').val(data.City);
        $('#adressUser').val(data.Adress);
        $('#statusUser').val(data.Position);
       // $('#passUser').val(data.Password); 
    }

    //заполняем селект
    function fillSelect(table, inp){
        inp.empty();
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOneAll",
                "table": table               
            },
                function(data){
                    var data = JSON.parse(data);
                    console.log(data);
                    data.forEach(function(item, i, data){
                    inp.append(`<option value="${item[0]}">${item[1]}</option>`);
                    })
                }
            );
    }

    //отправка
    function sendTovar(){
        var btn = $(this).attr('data-edit');
        var id = $(this).attr('data-id');
        var fname = $('#fnameUser').val();
        var lname = $('#L_fnameUser').val();
        var mail = $('#mailUser').val();
        var phone = $('#phoneUser').val();
        var city = $('#cityUser').val();
        var adress = $('#adressUser').val();
        var pass = $('#passUser').val();
        var position = $('#statusUser').val();
        if (btn === 'true')
        {
            console.log('data1');
             $.post(
            "templates/vendor/core.php",
            {
                "action"   : "editUser",
                "fname"     : fname,
                "lname"    : lname,
                "mail"     : mail,
                "phone"    : phone,
                "city"     : city,
                "adress"    : adress,
                "position" : position,
                "id"       : id
            },
                function(data){
                    console.log(data);
                    clearInput();
                  
                    init();
                }
            );
        }
        else
        if (btn === 'false')
        {   console.log('data');
            $.post(
            "templates/vendor/core.php",
            {
                "action"   : "addUser",
                "fname"     : fname,
                "lname"    : lname,
                "mail"     : mail,
                "phone" : phone,
                "city"    : city,
                "adress"    : adress,
                "pass"    : pass,
                "position":position             
            },
                function(data){
                    console.log(data);
                    clearInput();
                   
                    init();
                }
            );

        }
         $('.modal.in').modal('hide');
    }

    //загрузка таблицы
    function init(){
         $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOneAllAssoc",
                "table" : "viewusers"              
            },
            function(data){
                
                var data = JSON.parse(data);
                console.log(data);
                var out ='';
                out += `<thead>`;
                    for(var id in data[1])
                           out += `<th>${[id]}</th>`;
                out +=`</thead>
                <tbody>`;
                data.forEach(function(item,i,data){
                    out +=`<tr>`;
                    for (var id in item){
                       out += `<td>${item[id]}</td>`;

                    }

                    out +=`<td><button data-id="${item['Код']}" data-toggle="modal"  data-target="#modalUser" id="modaledit" type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                    <button data-id="${item['Код']}" id="modaldelete" fname="modaldelete" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></td></tr>`;
                })
          
                 out +=`</tbody>`;

                $('#main-table').html(out);
            }
        )
    }

    //удалить
	$(document).on('click', '#modaldelete', function(){
        if(confirm("Действительно удалить?"))
        {
            var id = $(this).attr('data-id');
            $.post(
                "templates/vendor/core.php",
                {
                    "action": "deleteTable",
                    "table": "users",
                    "id": id
                },
                function(data){
                    console.log(data);
                    init();
                }
                )
        }
    
    })

   

</script>