<?php session_start(); ?>
<?php 
      if(!$_SESSION['users'] and !$_SESSION['employee']):
?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-6">
       <div class="alert-danger">
          <?=$_SESSION['message'];?>
          <?php unset($_SESSION['message']) ?>
        </div>
      <form class="form-horizontal" role="form" method="POST" action="templates/vendor/signin.php">
        <div class="form-group">
          <label for="">Почта</label>
          <input type="email" class="form-control" placeholder="Почта" required name="login">
        </div>
        <div class="form-group" >
          <label for="">Пароль</label>
          <input type="password" maxlength="18" class="form-control" placeholder="Пароль" name="password">
        </div>
        <div class="form-group">
          <label for="" class="">Нет аккаунта? <a href="registration">Зарегестрироваться</a></label>
        </div>
        <div class="form-group">
           <input type="submit" class="btn btn-success" value="Войти">
        </div>
        <hr>
       
      </form>
    </div>
  </div>
</div>


<?php endif; 
    if($_SESSION['users'])
      echo '
      <script>
       document.location.href = "profile";
      </script>';
?>
     