<?php session_start();?>
<div class="container">
	<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>История покупок</h2>
                </div>
              
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
			<thead>
				<tr>
					<th>Номер заказа</th>
					<th>Наименование</th>
					<th>Дата заказа</th>
					<th>Цена</th>
					<th>Кол-во</th>
					<th>Сумма</th>
				</tr>
			</thead>
			<tbody id="main-cart">
				
			</tbody>
		</table>
        </div>       
    </div>

</div>

<script>
	var cart = {};
	var count;

	$(document).ready(function () {
	   	showCart();
	});

	function showCart() {
	    //вывод истории
	
	        $.post("templates/vendor/core.php", 
	        { 
	            "action" : "loadHistory",
	            "cart":cart     
	        },
	            function (data) {
	          	var data = JSON.parse(data);
	            $('#main-cart').html(data);
	          
	        });
	    
	}


	function isEmpty(object) {
	    //проверка корзины на пустоту

	    for (var key in object)
	    if (object.hasOwnProperty(key)) return true;
	    return false;
	}

</script>