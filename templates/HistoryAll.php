<?php session_start();?>
<div class="container">
	<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>История покупок</h2>
                </div>
   
            </div>
        </div>        
        <div class="card-body">
        	<div>
        		<label for=""><input type="search" id="search" class="form-control form-control-sm" placeholder="Поиск"></label>
        	</div>
        	<div>
        		<table class="table table-striped">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Наименование</th>
							<th>Пользователь</th>
							<th>Дата заказа</th>
							<th>Цена</th>
							<th>Кол-во</th>
							<th>Сумма</th>
						</tr>
					</thead>
					<tbody id="main-cart">
						
					</tbody>
				</table>
        	</div>
            
        </div>       
    </div>

</div>

<script>
	var cart = {};
	var count;

	$(document).ready(function () {
	   	init();
	});

	function init() {
	    //вывод истории
	
	        $.post("templates/vendor/core.php", 
	        { 
	            "action" : "loadHistoryAll"
	                
	        },
	            function (data) {
	         	var data = JSON.parse(data);
	           	console.log(data);
	            $('#main-cart').html(data);
	           /* if (!isEmpty(data)) {
	        	$('#main-cart').html('<div class="row" align="center"><h3>Пока нет покупок!<a href="product">Товары</a></h3></div>');      
	           	}*/
	        });
	    
	}


	function isEmpty(object) {
	    //проверка корзины на пустоту

	    for (var key in object)
	    if (object.hasOwnProperty(key)) return true;
	    return false;
	}

	$(document).on('input','#search',function(e){
		console.log($(this).val());
		$('#main-cart').html("");
		
			$.post(
					"templates/vendor/core.php",
					{
					"action": "loadHistoryAll",
					"txt": $(this).val()
					},
					function(data){
						console.log('вызов');
						var data = JSON.parse(data);
						console.log(data);
						
						$('#main-cart').html(data);
			})
	});
</script>