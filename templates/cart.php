<?php session_start();?>
<div class="container">
	<div class="row">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Товар</th>
					<th>Цена</th>
				</tr>
			</thead>
			<tbody id="main-cart">
				
			</tbody>
		</table>
		<button class="btn btn-info col-auto" id="order">Оформить заказ</button>
		<div id="order-menu" hidden>
			
			<div class="form-froup mb-1"><label for="">Имя</label><input type="text" class="form-control" id="namein"></div>
			<div class="form-froup mb-1"><label for="">Телефон</label><input type="text" class="form-control" id="phonein"></div>
			<div class="form-froup mb-1"><label for="">Город</label><input type="text" class="form-control" id="cityin"></div>
			<div class="form-froup mb-1"><label for="">Адрес</label><input type="text" class="form-control" id="adressin"></div>
			<div class="form-froup mb-1"><label for="">Электронная почта</label><input type="text" class="form-control" id="malein"></div>
			<button class="btn bg-info d-flex" id="send-order" data-method="serialize">Отправить</button>
			
		</div>
	</div>
</div>

<script>
	var cart = {};
	var count;

	$(document).ready(function () {
	   	loadCart();
	   	$(document).on('click', '#deleteitem', delGoods);
		$(document).on('click', '#order', showOrder);
		$(document).on('click','#send-order', addOrder);
	});
	
	//Разворачиваем поле оформления
	function showOrder(){
		$('#order-menu').prop('hidden', false);
		$(this).hide();
	}

	//Оформление заказа
	function addOrder(){
		var name = $('#namein').val();
		var phone = $('#phonein').val();
		var city = $('#cityin').val();
		var adress = $('#adressin').val();
		var mail = $('#malein').val();

		if (isEmpty(cart)){
		 $.post("templates/vendor/core.php", 
	        { 
	            "action" : "addOrder",
	            "cart": cart,
	            "name": name,
	            "phone" : phone,
	            "city"  : city,
	            "adress" : adress,
	            "mail"  : mail,
	            "total": $('#total').text()     
	        },
	            function (data) {
	           	console.log(data);
	           	deleteCart();
	           	alert("Заявка подана успешно!");
	        });
		}
		else
		{
		 alert("Корзина пустая");	
		}
		
	}
	

	function loadCart() {
	    //проверяю есть ли в localStorage запись cart
	    if (localStorage.getItem('cart')) {
	        // если есть - расширфровываю и записываю в переменную cart
	        cart = JSON.parse(localStorage.getItem('cart'));
	        setCount();
	        showCart();
	        }
	    else {
	        $('#main-cart').html('<div class="row">Корзина пуста!</div>');
	    }
	}


	function showCart() {
	    //вывод корзины
	    
	    if (!isEmpty(cart)) {
	        count = 0;
	        $('#cart-count').html(count);        
	        $('#main-cart').html('<div><h3>Услуги не выбраны!</h3></div>');
	        saveCartCount();
	    }
	    else {

	        $.post("templates/vendor/core.php", 
	        { 
	            "action" : "loadCartProduct",
	            "cart": cart     
	        },
	            function (data) {
	          	var data = JSON.parse(data);
	          	console.log(data.length);
				var out ='';
				for (var key in data)
				out += `<tr>
						<td><a href="service?id=${data[key][0]}" class="text-decoration-none">${data[key][1]}</a></td>
						<td>${data[key][2]}</td>
						<td>
						<button id="deleteitem" class="btn btn-danger" data-id="${data[key][0]}"><i class="fa fa-trash"></i></button></td>";
						</tr>;`;
				out += `<tr><td>Всего: ${data.length}</td><td></td><td></td></tr>`
	            $('#main-cart').html(out);

	        });
	    }
	}

	
	function deleteCart(){
		//Очищаем корзину после оформления заказа
	    cart = {};
	    saveCart();
	    showCart();
	}

	
	function delGoods() {
	    //удаляем товар из корзины

	    var id = $(this).attr('data-id');
	    count = count - cart[id];
	    delete cart[id];
	    saveCartCount();
	    saveCart();
	    showCart();
	    setCount();
	}

	

	
</script>