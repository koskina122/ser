<?php  
session_start();
require 'vendor/function.php';
global $link;
$id = $_GET['id'];

$sql = "select * from services where id = $id;";
$result = $link->query($sql);


?>

<div class="container">
	<?php while($row = $result -> fetch_array())
	{
	?>
	<div class="row g-0">
	    <div class="col-md-4">
	      <img src="img/<?=$row[5]?>" alt="" class="rounded img-thumbnail">
	    </div>
	    <div class="col-md-8">
	      	<div class="card-body" data-serviceId="<?=$row[0]?>">
		        <h5 class="card-title"><?=$row[1]?></h5>
		        <p class="card-text">Стоимость: <?=$row[2]?></p>
		        <h5 class="card-text">Описание</h5>
		        <p class="card-text"><?=$row[4]?></p>
	      	</div>
	    </div>
  	</div>
	<? }?>
	<div class="d-flex justify-content-center mt-5">
		<div class="col-10">
			<div class="content-slider">
			</div>
		</div>
	</div>
	
</div>
<div class="container mt-5">
	<div class="row">
		<p class="" data-id="<?=$id?>" id="commentP">
			Комментарии
		</p>
	</div>
		<?php 
			if (!$_SESSION['users'])
			{
				echo "Авторизуйтесь, чтобы оставить комментарий.";
			}
		?>	
		<?php if($_SESSION['users']):?>
		<div class="row">
			<form action="">
				<div class="form-group">
					<p><a href="" class="text-decoration-none">Фамилия Имя</a></p>
					<textarea style="resize: none;" class="form-control" id="commentArea"></textarea>
				</div>	
				 <div class="form-group row">
			    	<div class="col-sm-10">
			      		<button type="button"  class="btn btn-primary" id="commentBtn">Отправить</button>
			    	</div>
				</div>
			</form>
		</div>
	<?php endif; ?>
		<a href="&page=2">tik</a>
		<div class="" id="comments-block">
			
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
        var serviceId = $('.card-body').attr('data-serviceId');
        
        init();
        loadGallery(serviceId);
        })

	$(document).on('click', '#commentBtn', function(){
		sendComments();
		console.log('OK');
	});
	//initialize
	function init(){
		var serviceId = $('#commentP').attr('data-id');
		$.post(
			"templates/vendor/core.php",
			
			{
				"action"   : "selectComments",
				"serviceId": serviceId
			},
                viewComments
                
        );
	}

	function isEmpty(object) {
	    //проверка корзины на пустоту

	    for (var key in object)
	    if (object.hasOwnProperty(key)) return true;
	    return false;
	}
	//Вывод комментариев
	function viewComments(data){
		data = JSON.parse(data);
		console.log(data);
		var out='';
		for (var key in data) {
		out += `<div class="form-group comment mb-2">
					<p class="header"><a href="" class="text-decoration-none">${data[key][1]}</a></p>
					<p>${data[key][2]}</p>
			</div>	`;
		}
		
		$('#comments-block').html(out);
	}

	//Отправка комментария
	function sendComments(){
        var serviceId = $('#commentP').attr('data-id');
        var comments = $('#commentArea').val();
       	
        $.post(
            "templates/vendor/core.php",
            {
                "action"   : "sendComments",
                "serviceId": serviceId,
                "comments" : comments
            },
                function(data){
                    console.log(data);
                    clearInput();
                    init();
                }
            );
    }
     
    //Очистка ввода комментария   
    function clearInput(){
        $('textarea').val("");
        $('#image').html("");
    }
</script>