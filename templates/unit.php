<?php session_start();?>
<div class="container">
	<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>Единицы</h2>
                </div>
                <div class="col-md-1">
          		 <button type="button" name="addTovar" data-toggle="modal" data-target="#modalEx" id="addTovar" class="btn btn-success btn"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
			<thead>
				<tr>
					<th>Наименование</th>
					<th>Описание</th>
				</tr>
			</thead>
			<tbody id="main-cart">
				
			</tbody>
		</table>
        </div>       
    </div>

</div>

<div class="modal fade" id="modalEx" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Редактирование</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Наименование 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="nameUnit" id="nameUnit" class="form-control" placeholder="Наименование" required >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Описание 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="opisanieUnit" id="opisanieUnit" class="form-control"  placeholder="Описание" required>
                            </div>
                        </div>
                    </div>
                    
                </form>  
            </div>
            <div class="modal-footer">
              <a href="" data-dismiss="modal" class="btn">Отмена</a>
              <a class="btn btn-success" id="wrckbtn" data-edit="false" data-dismiss="modal">Ок</a>
            </div>
          </div>
        </div>
    </div>

<script>
	var cart = {};
	var count;

	$(document).ready(function () {
	   	init();
	});

	$(document).on('click', '#modaledit', function(){
            var id = $(this).attr('data-id');

            clearInput();
       
            loadOne('unit', id, setTovar);
            $('#wrckbtn').attr('data-id', id);
            $('#wrckbtn').attr('data-edit', 'true');
           // $('#modalEx').modal('toggle');
            console.log('tik');
        });

        $(document).on('click', '#addTovar', function(){
            clearInput();
            $('.modal-title').text('Добавить');

            $('#wrckbtn').attr('data-edit', 'false');
         });

        $(document).on('click', '#wrckbtn', sendTovar);


    function loadOne(table, id, func){
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOne",
                "table": table,
                "id": id                
            },
                func
            );
    }


    //устанавливаем поля для редактирования товара
    function setTovar(data){
        data = JSON.parse(data);
        $('#nameUnit').val(data[1]);
        $('#opisanieUnit').val(data[2]);
        //$('#fileTovar').val(data[7]); 
    }

    //Очистка инпута    
    function clearInput(){
        $('input').val("");
    }

	function init() {
	    //вывод истории
	
	        $.post("templates/vendor/core.php", 
	        { 
	            "action" : "loadUnit",
	            "cart":cart     
	        },
	            function (data) {
	          	var data = JSON.parse(data);
	            $('#main-cart').html(data);
	          
	        });
	    
	}

	function sendTovar(){
        var btn = $(this).attr('data-edit');
        var id = $(this).attr('data-id');
        var name = $('#nameUnit').val();
        var opisanie = $('#opisanieUnit').val();
        if (btn === 'true')
        {
            console.log('data1');
             $.post(
            "templates/vendor/core.php",
            {
                "action"   : "editUnit",
                "name"     : name,
                "opisanie" : opisanie,
                "id"       : id
            },
                function(data){
                    console.log(data);
                    clearInput();
                    init();
                }
            );
        }
        else
        if (btn === 'false')
        {   console.log('data');
            $.post(
            "templates/vendor/core.php",
            {
                "action"   : "addUnit",
                "name"     : name,
                "opisanie" : opisanie          
            },
                function(data){
                    console.log(data);
                    clearInput();
                   
                    init();
                }
            );
            
          //  $('.modal.in').modal({show: false});

        }
    }

	 //удалить
	$(document).on('click', '#modaldelete', function(){
        if(confirm("Действительно удалить?"))
        {
            var id = $(this).attr('data-id');
            $.post(
                "templates/vendor/core.php",
                {
                    "action": "deleteTable",
                    "table": "unit",
                    "id": id
                },
                function(data){
                    console.log(data);
                    init();
                }
                )
        }
    
    })

</script>