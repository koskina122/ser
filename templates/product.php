<?php session_start(); 
require 'vendor/connection.php';
/*
if(isset($_GET['page']))
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = 1;
    }
   
*/
?>
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>Товары</h2>
                </div>
                <div class="col-md-1 text-right">
                    <button type="button" name="addTovar" data-toggle="modal" data-target="#modalEx" id="addTovar" class="btn btn-success btn"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div>
                <label for=""><input type="search" id="search" class="form-control form-control-sm" placeholder="Поиск"></label>
            </div>
            <table class="table table-striped" id="main-table"></table>
        </div>       
    </div>
	
    <!--
    
    <div class="modal fade" id="modalEx" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Редактирование</h4>
            </div>
            <div class="modal-body">
                <form onsubmit="return false";  enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Наименование 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="nameTovar" id="nameTovar" class="form-control" placeholder="Наименование" required >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Цена 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="priceTovar" id="priceTovar" class="form-control"  placeholder="Цена" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Единица 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <select name="unitTovar" id="unitTovar" class="custom-select" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Категория 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <select name="categoryTovar" id="categoryTovar">
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Количество 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="number"  placeholder="Количество" name="countTovar" id="countTovar" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Годен до: 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="date" name="goodTovar" id="goodTovar" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Изображение 
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="file" name="fileTovar" id="fileTovar" class="form-control" required>
                                 <span name="image" id="image"></span>
                            </div>

                        </div>
                    </div> 
                </form>  
            </div>
            <div class="modal-footer">
              <a href="" data-dismiss="modal" class="btn">Отмена</a>
              <a class="btn btn-success" id="wrckbtn" data-edit="false" data-dismiss="modal">Ок</a>
            </div>
          </div>
        </div>
    </div>
-->
</div>

<script>
	$(document).ready(function(){
        init();
        $(document).on('click', '#modaledit', function(){
            var id = $(this).attr('data-id');

            clearInput();
            fillSelect('category', $('#categoryTovar'));
            fillSelect('unit', $('#unitTovar')); 
            loadOne('product', id, setTovar);
            $('#wrckbtn').attr('data-id', id);
            $('#wrckbtn').attr('data-edit', 'true');
           // $('#modalEx').modal('toggle');
            console.log('tik');
        });

        $(document).on('click', '#addTovar', function(){
            clearInput();
            $('.modal-title').text('Добавить');

            fillSelect('category', $('#categoryTovar'));
            fillSelect('unit', $('#unitTovar'));

            $('#wrckbtn').attr('data-edit', 'false');
         });

        $(document).on('click', '#wrckbtn', sendTovar);
	});

    //Получаем одну запись
    function loadOne(table, id, func){
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOne",
                "table": table,
                "id": id                
            },
                func
            );
    }

    function clearInput(){
        $('input').val("");
        $('#image').html("");
    }

    //устанавливаем поля для редактирования товара
    function setTovar(data){
        data = JSON.parse(data);
        $('#nameTovar').val(data[1]);
        $('#priceTovar').val(data[2]);
        $('#unitTovar').val(data[3]);
        $('#categoryTovar').val(data[4]);
        $('#countTovar').val(data[5]);
        $('#goodTovar').val(data.Valid);
        $('#image').html(data.Image);
        //$('#fileTovar').val(data[7]); 
    }

    //заполняем селект
    function fillSelect(table, inp){
        inp.empty();
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOneAll",
                "table": table
                        
            },
                function(data){
                    var data = JSON.parse(data);
                    console.log(data);
                    data.forEach(function(item, i, data){
                    inp.append(`<option value="${item[0]}">${item[1]}</option>`);
                    })
                }
            );
    }

    //отправка
    function sendTovar(){

        var formData = new FormData();
        var files = $('#fileTovar')[0].files;
       

        var btn = $(this).attr('data-edit');
        var id = $(this).attr('data-id');
        var name = $('#nameTovar').val();
        var price = $('#priceTovar').val();
        var unit = $('#unitTovar').val();
        var category = $('#categoryTovar').val();
        var count = $('#countTovar').val();
        var valid = $('#goodTovar').val();
        var image = $('#image').text();
        formData.append('name', name);
        formData.append('price', price);
        formData.append('unit', unit);
        formData.append('category', category);
        formData.append('count', count);
        formData.append('valid', valid);
        formData.append('id', id);
        formData.append('image', image);
         if(files.length > 0 ){
           formData.append('file',files[0]);
            }   

             console.log(formData);
        if (btn === 'true')
        {
            console.log('data1');

                        $.ajax({
              url: 'templates/vendor/core.php?action=editTovar',
              type: 'POST',
              data:formData,
              contentType: false,
              processData: false,
              success: function(data){
              //    var data = JSON.parse(data);
                console.log(data);
                 clearInput();
                init();
              },
           });

        }
        else
        if (btn === 'false')
        {   console.log('data');


            $.ajax({
              url: 'templates/vendor/core.php?action=addTovar',
              type: 'POST',
              data:formData,
              contentType: false,
              processData: false,
              success: function(data){
              //    var data = JSON.parse(data);
                console.log(data);
                 clearInput();
                init();
              },
           });

        }
        
    }

    //загрузка таблицы
    function init(){
         $.post(
            "templates/vendor/core.php",
            {
                "action": "loadProductAll"                
            },
            function(data){
                
                var data = JSON.parse(data);
                console.log(data);
                var out ='';
                out += `<thead>`;
                    for(var id in data[1])
                           out += `<th>${[id]}</th>`;
                out +=`</thead>
                <tbody>`;
                data.forEach(function(item,i,data){
                    out +=`<tr>`;
                    for (var id in item){
                       out += `<td>${item[id]}</td>`;

                    }

                    out +=`<td><button data-id="${item['Код']}" data-toggle="modal"  data-target="#modalEx" id="modaledit" type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                    <button data-id="${item['Код']}" id="modaldelete" name="modaldelete" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></td></tr>`;
                })
          
                 out +=`</tbody>`;

                $('#main-table').html(out);
            }
        )
    }

    //ПОИСК
    $(document).on('input','#search',function(e){
        console.log($(this).val());
        $('#main-table').html("");
        
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadProductAll",
                "txt" : $(this).val()                
            },
            function(data){
                
                var data = JSON.parse(data);
                console.log(data);
                var out ='';
                out += `<thead>`;
                    for(var id in data[1])
                           out += `<th>${[id]}</th>`;
                out +=`</thead>
                <tbody>`;
                data.forEach(function(item,i,data){
                    out +=`<tr>`;
                    for (var id in item){
                       out += `<td>${item[id]}</td>`;

                    }

                    out +=`<td><button data-id="${item['Код']}" data-toggle="modal"  data-target="#modalEx" id="modaledit" type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                    <button data-id="${item['Код']}" id="modaldelete" name="modaldelete" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></td></tr>`;
                })
          
                 out +=`</tbody>`;

                $('#main-table').html(out);
            }
        )
    });


    //удалить
	$(document).on('click', '#modaldelete', function(){
        if(confirm("Действительно удалить?"))
        {
            var id = $(this).attr('data-id');
            $.post(
                "templates/vendor/core.php",
                {
                    "action": "deleteTable",
                    "table": "product",
                    "id": id
                },
                function(data){
                    console.log(data);
                    init();
                }
                )
        }
    
    })

   

</script>