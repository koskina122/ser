<?php session_start(); ?>
<?php 
     
      if(!$_SESSION['users']):
?>
<div class="container">
  <div class="form">
    <div class="col-sm-offset-2">
        <label>Введити логин и придумайте пароль</label>
    </div>
  
    <form class="form-horizontal" role="form" method="POST" action="templates/vendor/signup.php">
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Почта</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" placeholder="example@mail.ru" name="mail" required>
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" placeholder="Пароль" name="password" required>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
         
            <label>
              Есть аккаунт? -><a  class="" href="autorisation">Войти</a>
            </label>
         
        </div>
      </div> 
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-success">Регистрация</button>
        </div>
      </div>
    </form>
  </div><!-- form  -->
</div>
<?php endif; ?>