<?php session_start(); ?>
<div class="container">
	<div class="row">
		<div class="card" style="width: 20rem;">
			  <div class="card-body">
			    <h5 class="card-title"><? echo $_SESSION['users']['lname'].' '.$_SESSION['users']['fname']; ?></h5>
			    <p class="card-text">
			    	<span>Почта: <?=$_SESSION['users']['mail']?></span> <br>
			    	<span>Телефон: <?=$_SESSION['users']['phone']?></span>
			    </p>
			    <a id="" data-id="<?=$_SESSION['users']['id']?>" class="btn btn-primary">Редактировать</a>
			  </div>
		</div>
		<div class="col-md-6">
			<div class="card" style="width: 20rem;">
			  <div class="card-body">
			    <h5 class="card-title">История покупок</h5>
			    <p class="card-text">
			    	<span>Всего покупок: </span> <br>
			    </p>
			    <a id="" data-id="<?=$_SESSION['users']['id']?>" class="btn btn-primary">Подробнее</a>
			  </div>
			</div>
		</div>

	</div> <!--row-->
</div>