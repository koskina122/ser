<div class="container">
	<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h2>Услуги</h2>
                </div>
                <div class="col-5">
                	<div class="input-group">
				  		<div class="form-outline">
				    	<input type="search" id="form1" class="form-control" />
				  	</div>
				  	<button type="button" class="btn btn-dark">
				    	<i class="fa fa-search"></i>
				  	</button>
					</div>
                </div>
                <div class="col-1">
          			<button type="button" name="addServices" data-toggle="modal" data-target="#modalEx" id="addServices" class="btn btn-success"><i class="fa fa-plus"></i></button>
            	</div>
        	</div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
			<thead>
				<tr>
                    <th></th>
					<th>Наименование</th>
					<th>Стоимость</th>
					<th>Описание</th>
					<th>Категория</th>
				</tr>
			</thead>
			<tbody  id="main-cart">
				
			</tbody>
		</table>
        </div>       
    </div>
</div>

<div class="modal fade" id="modalEx" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Редактирование</h4>
            </div>
            <div class="modal-body">
                <form id="forms" method="POST"  enctype="multipart/form-data">
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Наименование 
                               
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="nameServices" id="nameServices" class="form-control" placeholder="Наименование" required >
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Стоимость 
                             
                            </label>
                            <div class="col-md-8">
                                <input type="text" name="costServices" id="costServices" class="form-control"  placeholder="Стоимость" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                            Единица 
                               
                            </label>
                            <div class="col-md-8">
                                <select class="form-select" name="unitOfMeasure" id="unitOfMeasure">
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Категория
                               
                            </label>
                            <div class="col-md-8">
                                <select class="form-select" name="categoryServices" id="categoryServices">
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Описание 
                                
                            </label>
                            <div class="col-md-8">
                                <textarea style="resize: none;" type="text" name="opisanieServices" id="opisanieServices" class="form-control"  placeholder="Описание" required></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group mb-2">
                        <div class="row">
                            <label for="" class="col-md-4 text-right">
                                Изображение
                               
                            </label>
                            <div class="col-md-8">
                                 <input name="imageServices" id="imageServices" type="file" class="form-control">
                                <span name="image" id="image">ыв</span>
                            </div>
                          </div>  
                    </div>
                    
                 
            </div>
            <div class="modal-footer">
              <a href="" data-dismiss="modal" class="btn">Отмена</a>
              <button type="submit" class="btn btn-success" id="wrckbtn" data-edit="false" data-dismiss="modal">Ок</button>

            </div>
            </form> 
          </div>
        </div>
    </div>

<script>
	var cart = {};
	var count;

	$(document).ready(function () {
	   	init();
	});

	$(document).on('click', '#modaledit', function(){
            var id = $(this).attr('data-id');
            clearInput();
       		fillSelect('Category', $('#categoryServices'));
       		fillSelect('Units', $('#unitOfMeasure'));
            loadOne('Services', id, setServices);
            $('#wrckbtn').attr('data-id', id);
            $('#wrckbtn').attr('data-edit', 'true');
        });

    $(document).on('click', '#addServices', function(){
            clearInput();
            $('.modal-title').text('Добавить');
            fillSelect('Category', $('#categoryServices'));
            fillSelect('Units', $('#unitOfMeasure'));
            $('#wrckbtn').attr('data-edit', 'false');
         });

    $(document).on("submit", "form", function(event)
    {
        event.preventDefault();
        
           var btn = $('#wrckbtn').attr('data-edit');
           
           
           /*
        if (btn === 'false')
            {   $.ajax({
                url: "templates/vendor/core.php?action=addServices",
                type: "POST",
                dataType: "html",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data, status)
                {

                    console.log(status);
                    console.log(data);
                },
                error: function (xhr, desc, err)
                {
                    
                    console.log(xhr);
                    console.log(err);
                }
            });        
            }
*/
    });


    function fillSelect(table, inp){
        inp.empty();
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOneAll",
                "table": table
                        
            },
                function(data){
                    var data = JSON.parse(data);
                    console.log(data);
                    data.forEach(function(item, i, data){
                    inp.append(`<option value="${item[0]}">${item[1]}</option>`);
                    })
                }
            );
    }

    //устанавливаем поля для редактирования товара
    function setServices(data){
        data = JSON.parse(data);
        console.log(data);
        $('#nameServices').val(data[1]);
        $('#costServices').val(data[2]);
      	$('#categoryServices').val(data[3]);
        $('#opisanieServices').val(data[4]);
        $('#image').html(data[5]);
    }

    //Очистка инпута    
    function clearInput(){
        $('input').val("");
    }

	function init() {
	    //инициализация
	        $.post("templates/vendor/core.php", 
	        { 
	            "action" : "loadService"  
	        },
	            function (data) {
	            console.log(data);
	          	var data = JSON.parse(data);
	            viewService(data);
	          
	        });
	    
	}

	function sendServices(){
        var form = $('#forms');

        var btn = $(this).attr('data-edit');
        var id = $(this).attr('data-id');

        if (btn === 'false')
        {
            $.ajax({
                url: "templates/vendor/core.php",
                type: "POST",
                data:{ 
                    action : "addServices", 
                    fdata  : new FormData(form[0])
                    },
                processData: false,
                contentType: false,
                success: function(data, status){
                    console.log(data);
                    console.log(status);
                }
            })
        }
               // if(files.length > 0 ){
         //  formData.append('file',files[0]);
         //   }   
      
             /*
        if (btn === 'true')
        {
            console.log('data1');
             $.post(
            "templates/vendor/core.php",
            {
                "action"   : "editServices",
                "name"     : name,
                "cost"	   : cost,
                "category" : category,
                "opisanie" : opisanie,
                "id"       : id
            },
                function(data){
                    console.log(data);
                    clearInput();
                    init();
                }
            );
        }
        else
        if (btn === 'false')
        {   console.log('data');
            $.post(
            "templates/vendor/core.php",
            {
                "action"   : "addServices",
                "name"     : name,
                "cost"	   : cost,
                "category" : category,
                "opisanie" : opisanie,

            },
                function(data){
                    console.log(data);
                    clearInput();
                   
                    init();
                }
            );
            
          //  $('.modal.in').modal({show: false});

        }*/
    }

	 //удалить
	$(document).on('click', '#modaldelete', function(){
        if(confirm("Действительно удалить?"))
        {
            var id = $(this).attr('data-id');
            $.post(
                "templates/vendor/core.php",
                {
                    "action": "deleteTable",
                    "table": "Services",
                    "id": id
                },
                function(data){
                    console.log(data);
                    init();
                }
                )
        }
    
    })

    function viewService(data){
        var out = '';
        for (var key in data)
        out += `<tr>
                    <td><img style="width: 40px; height: 30px" src="img/${data[key][6]}"></td>
                    <td><a href="service?id=${data[key][0]}">${data[key][1]}</a></td>
                    <td>${data[key][2]}</td>
                    <td>${data[key][5]}</td>
                    <td>${data[key][4]}</td>

                    <td><button data-id="${data[key][0]}" data-toggle="modal"  data-target="#modalEx" id="modaledit" type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                    <button data-id="${data[key][0]}" id="modaldelete" name="modaldelete" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                        
                </tr>`;
        $('#main-cart').html(out);
    }

</script>