<div class="container">
	<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h2>Заявки</h2>
                </div>
                <div class="col-5">
                	<div class="input-group">
				  		<div class="form-outline">
				    	<input type="search" id="form1" class="form-control" />
				  	</div>
				  	<button type="button" class="btn btn-dark">
				    	<i class="fa fa-search"></i>
				  	</button>
					</div>
                </div>
                <div class="col-1">
          			
            	</div>
        	</div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="content-table">
				<tbody  id="main-cart">
				
				</tbody>
			</table>
        </div>       
    </div>
</div>
<script>
	$(document).ready(function(){
		loadOrders();
	})
</script>