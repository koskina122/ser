<?php session_start(); ?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Строительные услуги</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/myjs.js"></script>
    <script src="js/slick.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet"  href="css/bootstrap-grid.min.css">
    <link rel="stylesheet"  href="css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet"  href="css/bootstrap-utilities.min.css">
    
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="shortcut icon" href="img/shorticon.ico">
</head>

<body style="margin-top: 90px;">
<nav class="navbar navbar-expand navbar-dark fixed-top bg-dark">
    <div class="container">
        <h3 class="mb-0"><a href="/" class="text-decoration-none header-title">Строительные услуги</a></h3>
        <div class="d-flex flex-column flex-wrap" id="navbarCollapse">
            <span class="navbar-text ml-auto py-0 px-lg-2">+375 25 375 375 375</span>
            <ul class="navbar-nav mb-auto mt-0 ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link py-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Кабинет</a>  

                    <div class="dropdown-menu">
                         <?php if ($_SESSION['users']):?>
                        <a class="dropdown-item" href="<?php if(!$_SESSION['users'] and !$_SESSION['employee']) echo 'autorisation'; else echo 'profile'?>">Профиль</a>
                        <a class="dropdown-item" href="history">История услуг</a>
                        <a class="dropdown-item" href="message">Ваши заявки</a>
                        <a class="dropdown-item" href="notifications">Уведомления</a>
                        <?php endif; ?>
                        <div class="dropdown-divider"></div>
                        <?php if (!$_SESSION['users'])
                              { ?>
                         <a class="dropdown-item" href="autorisation">Войти</a>          
                        <?php } 
                            else 
                            { ?>                
                        <a class="dropdown-item" id = "logout">Выйти</a>
                            <?php } ?>
                    </div>
                </li>
<!--                <li>
                     <a href="cart" class="nav-link py-0">
                        Корзина <span class="badge badge-warning text-light" id="cartcount"></span>
                    </a>
                </li>
            -->
                <li class="">
                    <a class="nav-link py-0" href="admin">Admin</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
    


    <script>
        $(document).on('click', '#logout', function(e){
            e.preventDefault();
            $.post(
                "templates/vendor/logout.php",
                function(){
                    document.location.href="autorisation";
                }
            )
        })
    </script>