<?php 

mysqli_query($link, "CREATE PROCEDURE `CheckDiscountProduct` ()  NO SQL
Update product SET Price = Price/2, Discount = true
WHERE DATEDIFF(Valid, NOW()) = 1 and Discount is null;");

mysqli_query($link, "CREATE PROCEDURE `notValidProduct` ()  NO SQL
DELETE FROM product WHERE DATEDIFF(Valid, NOW()) < 0;");

mysqli_query($link, "CREATE PROCEDURE `userHistoryCount` (IN `Id` INT)  NO SQL
SELECT COUNT(Id) AS Количество from orders WHERE orders.Users = Id;");


mysqli_query($link, "CREATE TABLE `cart` (
  `Id` int(11) NOT NULL,
  `Product` int(11) DEFAULT NULL,
  `Count` int(11) NOT NULL,
  `Orders` int(11) NOT NULL,
  `Name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `Cost` decimal(10,2) NOT NULL
) ;");


mysqli_query($link, "INSERT INTO `cart` (`Id`, `Product`, `Count`, `Orders`, `Name`, `Price`, `Cost`) VALUES
(5, NULL, 4, 11, 'Семга', '13.60', '54.40'),
(6, NULL, 1, 11, 'Икра', '12.00', '12.00'),
(7, 6, 3, 11, 'Пельмени', '10.22', '30.66'),
(8, NULL, 5, 12, 'Икра', '12.00', '60.00');");


mysqli_query($link, "CREATE TABLE `category` (
  `Id` int(11) NOT NULL,
  `Name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Opisanie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ;");



mysqli_query($link, "INSERT INTO `category` (`Id`, `Name`, `Opisanie`) VALUES
(1, 'Молочные продукты', 'afsgd'),
(2, 'Рыбная продукция', 'dafs'),
(4, 'фаывп', 'фаымвип');");

 
mysqli_query($link, "CREATE TRIGGER `trDeleteCategory` BEFORE DELETE ON `category` FOR EACH ROW DELETE FROM product WHERE product.Category = OLD.Id
;");
 

mysqli_query($link, "CREATE TABLE `orders` (
  `Id` int(11) NOT NULL,
  `Users` int(11) NOT NULL,
  `Date_order` date NOT NULL,
  `Price` decimal(10,2) NOT NULL
) ;");

 
mysqli_query($link, "CREATE TRIGGER `trDeleteOrders` BEFORE DELETE ON `orders` FOR EACH ROW DELETE FROM cart WHERE cart.Orders = OLD.Id
;");
 

mysqli_query($link, "CREATE TABLE `position` (
  `Id` int(11) NOT NULL,
  `Name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ;");

mysqli_query($link, "INSERT INTO `position` (`Id`, `Name`) VALUES
(1, 'Администратор'),
(2, 'Менедежр'),
(3, 'Пользователь');
");

mysqli_query($link, "CREATE TABLE `product` (
  `Id` int(11) NOT NULL,
  `Name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `Unit` int(11) NOT NULL,
  `Category` int(11) NOT NULL,
  `Count` int(11) NOT NULL,
  `Image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Valid` date NOT NULL,
  `Discount` tinyint(1) DEFAULT NULL
) ;");

mysqli_query($link, "INSERT INTO `product` (`Id`, `Name`, `Price`, `Unit`, `Category`, `Count`, `Image`, `Valid`, `Discount`) VALUES
(6, 'Пельмени', '10.22', 1, 1, 200, 'default.img', '2020-12-26', NULL);");

mysqli_query($link, "CREATE TRIGGER `trUpdateProduct` AFTER UPDATE ON `product` FOR EACH ROW DELETE FROM product WHERE product.Count = 0
;");

mysqli_query($link, "CREATE TABLE `unit` (
  `Id` int(11) NOT NULL,
  `Name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Opisanie` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ;");

mysqli_query($link, "INSERT INTO `unit` (`Id`, `Name`, `Opisanie`) VALUES
(1, 'кг', 'килограммы');");
 
mysqli_query($link, "CREATE TRIGGER `trDeleteUnit` BEFORE DELETE ON `unit` FOR EACH ROW DELETE FROM product WHERE product.Unit = OLD.Id
;");
 
mysqli_query($link, "CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `F_Name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `L_Name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mail` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `City` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Adress` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Position` int(11) NOT NULL
) ;");


mysqli_query($link, "INSERT INTO `users` (`Id`, `F_Name`, `L_Name`, `Mail`, `Phone`, `Password`, `City`, `Adress`, `Position`) VALUES
(6, 'Ильюша', 'Петров', 'shvydkin2000@mail.ru', '375292483531', '827ccb0eea8a706c4c34a16891f84e7b', 'Vitebsk', 'Gorki', 2),
(9, 'Artemiy', 'Svydkin', 'shvydkin2@mail.ru', '0292483531', '827ccb0eea8a706c4c34a16891f84e7b', 'LA', 'Haharina 41a  204', 3),
(10, 'Admin', 'Admin', 'admin@mail.ru', '0292483531', 'b59c67bf196a4758191e42f76670ceba', 'LA', 'Haharina 41a  204', 1);");


mysqli_query($link, "CREATE TRIGGER `trDeleteUsers` BEFORE DELETE ON `users` FOR EACH ROW DELETE FROM orders WHERE orders.Users = OLD.Id
;");
 
mysqli_query($link, "CREATE VIEW `vieworders`  AS SELECT `cart`.`Id` AS `Id`, `cart`.`Product` AS `Код_Продукта`, `cart`.`Orders` AS `Номер заказа`, `users`.`Id` AS `ПользовательId`, `users`.`L_Name` AS `Пользователь`, `orders`.`Date_order` AS `Дата заказа`, `cart`.`Name` AS `Наименование`, `cart`.`Count` AS `Количество`, `cart`.`Price` AS `Цена`, `cart`.`Cost` AS `Сумма` FROM ((`cart` join `orders` on(`orders`.`Id` = `cart`.`Orders`)) join `users` on(`orders`.`Users` = `users`.`Id`)) ;");

mysqli_query($link, "CREATE VIEW `viewproduct`  AS SELECT `product`.`Id` AS `Код`, `product`.`Name` AS `Наименование`, `product`.`Price` AS `Цена`, `unit`.`Name` AS `Ед`, `category`.`Name` AS `Категория`, `product`.`Count` AS `Кол-во`, `product`.`Valid` AS `Годен до`, `product`.`Image` AS `Изображение` FROM ((`product` join `unit` on(`unit`.`Id` = `product`.`Unit`)) join `category` on(`category`.`Id` = `product`.`Category`)) ;");

mysqli_query($link, "CREATE VIEW `viewusers`  AS SELECT `users`.`Id` AS `Код`, `users`.`F_Name` AS `Има`, `users`.`L_Name` AS `Фамилия`, `users`.`Mail` AS `Почта`, `users`.`Phone` AS `Телефон`, `users`.`City` AS `Город` FROM `users` ;");

mysqli_query($link, "ALTER TABLE `cart`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Product` (`Product`,`Orders`),
  ADD KEY `Orders` (`Orders`);");

mysqli_query($link, "ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`);");


mysqli_query($link, "ALTER TABLE `orders`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Users` (`Users`);");

mysqli_query($link, "ALTER TABLE `position`
  ADD PRIMARY KEY (`Id`);");

mysqli_query($link, "ALTER TABLE `product`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Category` (`Category`),
  ADD KEY `Unit` (`Unit`);");

mysqli_query($link, "ALTER TABLE `unit`
  ADD PRIMARY KEY (`Id`);");

mysqli_query($link, "ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Position` (`Position`);");

mysqli_query($link, "ALTER TABLE `cart`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;");

mysqli_query($link, "ALTER TABLE `category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;");

mysqli_query($link, "ALTER TABLE `orders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;");

mysqli_query($link, "ALTER TABLE `position`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;");

mysqli_query($link, "ALTER TABLE `product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;");

mysqli_query($link, "ALTER TABLE `unit`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;");

mysqli_query($link, "ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;");

mysqli_query($link, "ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`Product`) REFERENCES `product` (`Id`) ON DELETE SET NULL;");

mysqli_query($link, "ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`Users`) REFERENCES `users` (`Id`);");

mysqli_query($link, "ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`Category`) REFERENCES `category` (`Id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`Unit`) REFERENCES `unit` (`Id`);");

mysqli_query($link, "ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`Position`) REFERENCES `position` (`Id`) ON DELETE CASCADE;");
 ?>