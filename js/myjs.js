function loadOne(table, id, func){
        $.post(
            "templates/vendor/core.php",
            {
                "action": "loadOne",
                "table": table,
                "id": id                
            },
                func
            );
    }

function loadGallery(serviceId){
    var query = "where serviceId = ";
    query = query.concat(serviceId);
    $.post(
        "templates/vendor/core.php",
        {
            "action"     : "loadOneAll",
            "table"        : "gallery",
            "query"        : query
        },
            function(data){
                data = JSON.parse(data);

                if (isEmpty(data)){
                    var out = `<div class="slider">`;
                    for(var key in data)
                        out += `<div class="slider_item"><img src="img/${data[key][1]}"></div>`;
                    out += `</div>`;
                    $('.content-slider').html(out);

                    $('.slider').slick({
                            arrows: true,
                            dots: true,
                            infinite: true,
                            slidesToShow: 2,
                            slidesToScroll: 2
                    });
                }
                
            }
    )
}

function loadOrders(){
     $.post(
        "templates/vendor/core.php",
        {
            "action"     : "loadOneAll",
            table        : "orders",
        },
        function(data){
           
            data = JSON.parse(data);
            console.log(data);
            var out = ``;
            for(var key in data)
            out += `<tr><td>${data[key][0]}</td>
            <td>${data[key][1]}</td>
            <td>${data[key][2]}</td>
            <td>${data[key][3]}</td>
            <td><button class="btn">Одобрить</button><button class="btn">Отказать</button></td>
            </tr>`;
        $('#main-cart').html(out);
        }
        );
}

function loadConfirmMessages($query){
    $.post(
       "templates/vendor/core.php",
       {
           "action"     : "loadOneAll",
           table        : "orders",
           "query"      : $query,
       },
       function(data){
          
           data = JSON.parse(data);
           console.log(data);
           var out = ``;
           for(var key in data)
           out += `<tr><td>${data[key][0]}</td>
           <td>${data[key][1]}</td>
           <td>${(data[key][3] === '1' && `Заявка подтверждена`) || `Заявка отклонена`}</td>
           </tr>`;
       $('#main-cart').html(out);
       }
       );
}


$(document).ready(function(){
    setCount();
})

            
function setCount(){
    $('#cartcount').html(localStorage.getItem('count'));
}


function showAlert(msg){    
    //оповещение
    $('#myalert').html('<div class="alert alert-success fixed-bottom" role="alert"><span id="msg"></span></div>');
    $('#msg').html(msg);
    $('#myalert').show('slow');
    setTimeout(function() { $("#myalert").hide('slow'); }, 2000);   
}

function saveCartCount(object) {
    //сохраняю корзину в localStorage
    localStorage.setItem('count', object); //корзину в строку   
}

function saveCart() {
    //сохраняю корзину в localStorage
    localStorage.setItem('cart', JSON.stringify(cart)); //корзину в строку
}

function isEmpty(object) {
    //проверка корзины на пустоту
    for (var key in object)
    if (object.hasOwnProperty(key)) return true;
    return false;
}