<?php 
session_start();

$route = $_GET['route'];

require 'templates/header.php';

switch ($route) {
    case '':
        require 'templates/main.php';
        break;

    case 'autorisation':
        require 'templates/autorisation.php';
        break;
    case 'orders':
	require 'templates/orders.php';
	break;
case 'message':
require 'templates/message.php';
break;
    case 'registration':
        require 'templates/registration.php';
        break;
    case 'notifications':
        require 'templates/notifications.php';
        break;

    case 'profile':
        require 'templates/profile.php';
        break;

    case 'cart':
        require 'templates/cart.php';
        break;

    case 'history':
        require 'templates/history.php';
        break;
 
    case 'service':
        require 'templates/serviceDetail.php';
        break;

    case 'users':
        require 'templates/users.php';
        break;

    case 'statistik':
        require 'templates/HistoryAll.php';
        break;

    case 'category':
        require 'templates/category.php';
        break;

    case 'admin':
        require 'templates/admin.php';
        break;
    case 'prof':
        require 'templates/admin.php';
        require 'templates/service.php';
        break;
    case 'dynamic':
        require 'templates/dynamic.php';
        break;

    default: 
        echo 'И что?';
        break;
}
require 'templates/footer.php';
 ?>