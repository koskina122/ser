-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 26 2021 г., 16:36
-- Версия сервера: 10.3.22-MariaDB-log
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `buildingjobs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `Id` int(11) NOT NULL,
  `Name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ParentCategory` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`Id`, `Name`, `ParentCategory`) VALUES
(2, 'Электрика', NULL),
(7, 'Сантехнические работы', NULL),
(8, 'Отделка', NULL),
(9, 'Фасадные работы', NULL),
(10, 'Штукатурные работы', NULL),
(11, 'Кровельные работы', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `Id` int(11) NOT NULL,
  `Service` int(11) NOT NULL,
  `User_Id` int(11) NOT NULL,
  `Text_Comments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateComments` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery`
--

CREATE TABLE `gallery` (
  `Id` int(11) NOT NULL,
  `Path` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ServiceId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `gallery`
--

INSERT INTO `gallery` (`Id`, `Path`, `ServiceId`) VALUES
(16, 'vanna3.jpg', 23),
(18, 'vn1.jpg', 23),
(20, 'vn2.jpg', 23);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `Id` int(11) NOT NULL,
  `Date_order` date NOT NULL,
  `ServiceId` int(11) NOT NULL,
  `ConfirmOrder` tinyint(1) NOT NULL DEFAULT 0,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`Id`, `Date_order`, `ServiceId`, `ConfirmOrder`, `userId`) VALUES
(4, '2021-05-11', 1, 0, 1),
(5, '2021-05-11', 2, 2, 1),
(6, '2021-05-11', 3, 0, 1),
(7, '2021-05-11', 4, 1, 1),
(8, '2021-05-11', 1, 2, 1),
(9, '2021-05-12', 1, 1, 1),
(10, '2021-05-12', 17, 1, 4),
(11, '2021-05-12', 23, 0, 4),
(12, '2021-05-12', 17, 0, 4),
(13, '2021-05-12', 17, 0, 4),
(14, '2021-05-12', 18, 0, 4),
(15, '2021-05-12', 18, 0, 4),
(16, '2021-05-12', 18, 0, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `ordersdetail`
--

CREATE TABLE `ordersdetail` (
  `Id` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `Service` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` varchar(160) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `name`, `cost`, `CategoryId`, `description`, `Image`) VALUES
(17, 'Укладка ламината', '12', 8, 'Укладка ламината', 'lami1.jpg'),
(18, 'Монтаж электропроводки', '45', 2, 'При отсутствии специальных знаний и подготовки монтаж электропроводки должен проводиться опытными специалистами. Наша компания обеспечит вашу безопасность и качество нашей работы', 'provod1.jpeg'),
(19, 'Монтаж фальцевой кровли', '45', 11, 'Монтаж фальцевой кровли', 'falc-s.jpg'),
(20, 'Отделка', '876', 8, 'При производстве отделочных работ мы используем только профессиональное оборудование и безопасные материалы, которые являются нетоксичными для человека', 'otdelka.jpg'),
(21, 'Установка унитаза', '45', 7, 'демонтаж старого и установка нового унитаза', 'unitaz.jpg'),
(23, 'Установка ванны', '45', 7, 'Демонтаж старой и установка новой ванны', 'vanna1.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `Email` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Adress` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Role` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`Id`, `Email`, `Password`, `Name`, `Adress`, `Phone`, `Role`) VALUES
(1, 'shvdkn@yandex.ru', '827ccb0eea8a706c4c34a16891f84e7b', 'Gremlin', NULL, NULL, 0),
(2, 'shvydkin2000@mail.ru', '827ccb0eea8a706c4c34a16891f84e7b', 'Artyom', NULL, NULL, 1),
(3, 'masha@mail.ru', '2fc4a68635c26db1019047965180ce1b', NULL, NULL, NULL, 1),
(4, 'kasha@mail.ru', '827ccb0eea8a706c4c34a16891f84e7b', NULL, NULL, NULL, 1),
(5, 'siri@gmail.com', 'b59c67bf196a4758191e42f76670ceba', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `viewcomments`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `viewcomments` (
`Service` int(11)
,`Name` varchar(20)
,`Text_Comments` text
);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `vieworders`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `vieworders` (
`Id` int(11)
,`Date_order` date
,`ServiceId` int(11)
,`ConfirmOrder` tinyint(1)
,`ServiceName` varchar(120)
,`UserName` varchar(20)
,`userId` int(11)
);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `viewservices`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `viewservices` (
`id` int(11)
,`name` varchar(120)
,`cost` decimal(10,0)
,`CategoryId` int(11)
,`CategoryName` varchar(80)
,`description` text
,`Image` varchar(160)
);

-- --------------------------------------------------------

--
-- Структура для представления `viewcomments`
--
DROP TABLE IF EXISTS `viewcomments`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewcomments`  AS  select `comments`.`Service` AS `Service`,`users`.`Name` AS `Name`,`comments`.`Text_Comments` AS `Text_Comments` from (`comments` join `users` on(`comments`.`User_Id` = `users`.`Id`)) ;

-- --------------------------------------------------------

--
-- Структура для представления `vieworders`
--
DROP TABLE IF EXISTS `vieworders`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vieworders`  AS  select `orders`.`Id` AS `Id`,`orders`.`Date_order` AS `Date_order`,`orders`.`ServiceId` AS `ServiceId`,`orders`.`ConfirmOrder` AS `ConfirmOrder`,`services`.`name` AS `ServiceName`,`users`.`Name` AS `UserName`,`orders`.`userId` AS `userId` from ((`orders` join `services` on(`orders`.`ServiceId` = `services`.`id`)) join `users` on(`orders`.`userId` = `users`.`Id`)) ;

-- --------------------------------------------------------

--
-- Структура для представления `viewservices`
--
DROP TABLE IF EXISTS `viewservices`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewservices`  AS  select `services`.`id` AS `id`,`services`.`name` AS `name`,`services`.`cost` AS `cost`,`services`.`CategoryId` AS `CategoryId`,`category`.`Name` AS `CategoryName`,`services`.`description` AS `description`,`services`.`Image` AS `Image` from (`services` join `category` on(`services`.`CategoryId` = `category`.`Id`)) ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Service` (`Service`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Индексы таблицы `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ServiceId` (`ServiceId`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `ordersdetail`
--
ALTER TABLE `ordersdetail`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CategoryId` (`CategoryId`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `gallery`
--
ALTER TABLE `gallery`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `ordersdetail`
--
ALTER TABLE `ordersdetail`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`Service`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`ServiceId`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ordersdetail`
--
ALTER TABLE `ordersdetail`
  ADD CONSTRAINT `ordersdetail_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `category` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
